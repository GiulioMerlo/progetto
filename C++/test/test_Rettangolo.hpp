#ifndef __TEST_RECTANGLE_H
#define __TEST_RECTANGLE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "PolygonCutter.hpp"

using namespace GeometryLib;
using namespace PolyCut;
using namespace testing;
using namespace std;

namespace CutTesting {

  TEST(TestRectangle, Test)
  {
    GeometryLibrary g;
    g.reset();
    PolygonCutter cutter = PolygonCutter(g);
    vector<pair<double, double>> points;
    vector<int> polygonVertices;
    tuple<double, double, double, double> segment;
    vector<pair<double, double>> newPoints;
    vector<vector<int>> cuttedPoly;

    points.resize(4);
    polygonVertices.resize(4);

    points[0].first = 1;
    points[0].second = 1;
    polygonVertices[0] = 0;
    points[1].first = 5;
    points[1].second = 1;
    polygonVertices[1] = 1;
    points[2].first = 5;
    points[2].second = 3.1;
    polygonVertices[2] = 2;
    points[3].first = 1;
    points[3].second = 3.1;
    polygonVertices[3] = 3;

    get<0>(segment) = 2.0;
    get<1>(segment) = 1.2;
    get<2>(segment) = 4.0;
    get<3>(segment) = 3.0;

    cuttedPoly = cutter.CutPolygon(points, polygonVertices, segment, newPoints);

    cout << "Cutted Polygons: " << endl;
    for(auto i = 0; i < cuttedPoly.size(); i++){
        for(auto j = 0; j < cuttedPoly[i].size(); j++){
            cout << cuttedPoly[i][j] << endl;
        }
        cout << "------------" << endl;
    }

    cout << endl << "New Points: " << endl;
    for(auto i = 0; i < newPoints.size(); i++){
        cout << "(" << newPoints[i].first << ", " << newPoints[i].second << ")" << endl;
    }
    cout << endl;


  }
}

#endif // __TEST_POLYCUT_H
