#ifndef __TEST_CONCAVE_H
#define __TEST_CONCAVE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "PolygonCutter.hpp"

using namespace GeometryLib;
using namespace PolyCut;
using namespace testing;
using namespace std;

namespace CutTesting {

  TEST(TestConcave, Test)
  {
    GeometryLibrary g;
    g.reset();
    PolygonCutter cutter = PolygonCutter(g);
    vector<pair<double, double>> points;
    vector<int> polygonVertices;
    tuple<double, double, double, double> segment;
    vector<pair<double, double>> newPoints;
    vector<vector<int>> cuttedPoly;

    points.resize(6);
    polygonVertices.resize(6);

    points[0].first = 1.5;
    points[0].second = 1;
    polygonVertices[0] = 0;
    points[1].first = 5.6;
    points[1].second = 1.5;
    polygonVertices[1] = 1;
    points[2].first = 5.5;
    points[2].second = 4.8;
    polygonVertices[2] = 2;
    points[3].first = 4.0;
    points[3].second = 6.2;
    polygonVertices[3] = 3;
    points[4].first = 3.2;
    points[4].second = 4.2;
    polygonVertices[4] = 4;
    points[5].first = 1;
    points[5].second = 4;
    polygonVertices[5] = 5;

    get<0>(segment) = 2;
    get<1>(segment) = 3.7;
    get<2>(segment) = 4.1;
    get<3>(segment) = 5.9;

    cuttedPoly = cutter.CutPolygon(points, polygonVertices, segment, newPoints);

    cout << "Cutted Polygons: " << endl;
    for(auto i = 0; i < cuttedPoly.size(); i++){
        for(auto j = 0; j < cuttedPoly[i].size(); j++){
            cout << cuttedPoly[i][j] << endl;
        }
        cout << "------------" << endl;
    }

    cout << endl << "New Points: " << endl;
    for(auto i = 0; i < newPoints.size(); i++){
        cout << "(" << newPoints[i].first << ", " << newPoints[i].second << ")" << endl;
    }
    cout << endl;


  }
}

#endif // __TEST_POLYCUT_H
