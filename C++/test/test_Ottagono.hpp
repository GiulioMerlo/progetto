#ifndef __TEST_OCTAGON_H
#define __TEST_OCATGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "PolygonCutter.hpp"

using namespace GeometryLib;
using namespace PolyCut;
using namespace testing;
using namespace std;

namespace CutTesting {

  TEST(TestOctagon, Test)
  {
    GeometryLibrary g;
    g.reset();
    PolygonCutter cutter = PolygonCutter(g);
    vector<pair<double, double>> points;
    vector<int> polygonVertices;
    tuple<double, double, double, double> segment;
    vector<pair<double, double>> newPoints;
    vector<vector<int>> cuttedPoly;

    points.resize(8);
    polygonVertices.resize(8);

    points[0].first = -6;
    points[0].second = -4;
    polygonVertices[0] = 0;
    points[1].first = -5;
    points[1].second = -5;
    polygonVertices[1] = 1;
    points[2].first = -2;
    points[2].second = -5;
    polygonVertices[2] = 2;
    points[3].first = -1;
    points[3].second = -4;
    polygonVertices[3] = 3;
    points[4].first = -1;
    points[4].second = -2;
    polygonVertices[4] = 4;
    points[5].first = -2;
    points[5].second = -1;
    polygonVertices[5] = 5;
    points[6].first = -5;
    points[6].second = -1;
    polygonVertices[6] = 6;
    points[7].first = -6;
    points[7].second = -2;
    polygonVertices[7] = 7;

    get<0>(segment) = -6;
    get<1>(segment) = -5;
    get<2>(segment) = -1;
    get<3>(segment) = -1;

    cuttedPoly = cutter.CutPolygon(points, polygonVertices, segment, newPoints);

    cout << "Cutted Polygons: " << endl;
    for(auto i = 0; i < cuttedPoly.size(); i++){
        for(auto j = 0; j < cuttedPoly[i].size(); j++){
            cout << cuttedPoly[i][j] << endl;
        }
        cout << "------------" << endl;
    }

    cout << endl << "New Points: " << endl;
    for(auto i = 0; i < newPoints.size(); i++){
        cout << "(" << newPoints[i].first << ", " << newPoints[i].second << ")" << endl;
    }
    cout << endl;


  }
}

#endif // __TEST_POLYCUT_H
