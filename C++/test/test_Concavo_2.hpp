#ifndef __TEST_CONCAVE_2_H
#define __TEST_CONCAVE_2_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "PolygonCutter.hpp"

using namespace GeometryLib;
using namespace PolyCut;
using namespace testing;
using namespace std;

namespace CutTesting {

  TEST(TestConcave2, Test)
  {
      GeometryLibrary g;
      g.reset();
      PolygonCutter cutter = PolygonCutter(g);
      vector<pair<double, double>> points;
      vector<int> polygonVertices;
      tuple<double, double, double, double> segment;
      vector<pair<double, double>> newPoints;
      vector<vector<int>> cuttedPoly;

      points.resize(6);
      polygonVertices.resize(6);

      points[0].first = 4;
      points[0].second = 5;
      polygonVertices[0] = 0;
      points[1].first = 1;
      points[1].second = 4;
      polygonVertices[1] = 1;
      points[2].first = 3;
      points[2].second = 2;
      polygonVertices[2] = 2;
      points[3].first = 2;
      points[3].second = 0;
      polygonVertices[3] = 3;
      points[4].first = 4;
      points[4].second = 2;
      polygonVertices[4] = 4;
      points[5].first = 2;
      points[5].second = 4;
      polygonVertices[5] = 5;

      get<0>(segment) = 2.5;
      get<1>(segment) = 3;
      get<2>(segment) = 2.5;
      get<3>(segment) = -1;

      cuttedPoly = cutter.CutPolygon(points, polygonVertices, segment, newPoints);

      cout << "Cutted Polygons" << endl;
      for(auto i = 0; i < cuttedPoly.size(); i++){
          for(auto j = 0; j < cuttedPoly[i].size(); j++){
              cout << cuttedPoly[i][j] << endl;
          }
          cout << "------------" << endl;
      }

      cout << endl << "New Points" << endl;
      for(auto i = 0; i < newPoints.size(); i++){
          cout << "(" << newPoints[i].first << ", " << newPoints[i].second << ")" << endl;
      }
  }
}

#endif // __TEST_CONCAVE_2_H
