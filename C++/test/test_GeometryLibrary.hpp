#ifndef __TEST_GEOMLIB_H
#define __TEST_GEOMLIB_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "GeometryLibrary.hpp"

using namespace GeometryLib;
using namespace testing;
using namespace std;

namespace GeometryTesting {

TEST(TestGeometryLib, TestPoint)
{
    GeometryLibrary g;
    g.reset();

    try
    {
        g.newPoint(2.4, 1.1, 0);
        g.newPoint(-6.0, 3.7, 1);
        Point p1 = g.getPoint(0);
        Point p2 = g.getPoint(1);
        EXPECT_EQ(p1.X, 2.4);
        EXPECT_EQ(p1.Y, 1.1);
        EXPECT_EQ(p2.X, -6.0);
        EXPECT_EQ(p2.Y, 3.7);
    }
    catch (const exception& exception)
    {
        FAIL();
    }

    try {
        g.newPoint(-8, 4, -1);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("A non negative id is required"));
    }

    try {
        g.newPoint(-8, 4, 1);
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Id is already used"));
    }

    try {
        g.getPoint(3);
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Point 3 does not exist"));
    }

}

TEST(TestGeometryLib, TestSegment)
{
    GeometryLibrary g;
    g.reset();

    g.newPoint(0.0, 1.1, 0);
    g.newPoint(2.3, 4.5, 1);
    g.newPoint(6.7, 8.9, 2);
    g.newPoint(-6, -1, 3);


    try {
        g.newSegment(0, 1, 0);
        g.newSegment(2, 3, 1);

        EXPECT_EQ(g.getSegment(0).Start, 0);
        EXPECT_EQ(g.getSegment(0).End, 1);
        EXPECT_EQ(g.getSegment(1).Start, 2);
        EXPECT_EQ(g.getSegment(1).End, 3);
    }
    catch (const exception& exception) {
        FAIL();
    }

    try {
        g.newSegment(0, 3, -2);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("A non negative id is required"));
    }

    try {
        g.newSegment(5, 7, 2);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("At least a point does not exist"));
    }

    try {
        g.newSegment(0, 3, 1);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Id is already used"));
    }

    try {
        g.newSegment(0, 0, 2);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Two distinct points are required"));
    }

    try {
        g.getSegment(3);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Segment 3 does not exist"));
    }
}

TEST(TestGeometryLib, TestPolygon){
    GeometryLibrary g;
    g.reset();

    vector<pair<double, double>> newPoints;
    vector<int> polygonVertices;
    Polygon poligono;
    Point punto;
    Segment segmento;

    newPoints.resize(3);
    polygonVertices.resize(3);

    newPoints[0].first = 0.0;
    newPoints[0].second = 0.0;
    polygonVertices[0] = 0;
    newPoints[1].first = 3.0;
    newPoints[1].second = 0.0;
    polygonVertices[1] = 1;
    newPoints[2].first = 1.5;
    newPoints[2].second = 1.0;
    polygonVertices[2] = 2;

    for(int i = 0; i < (int)newPoints.size(); i++){
        g.newPoint(newPoints[i].first, newPoints[i].second, polygonVertices[i]);
    }

    try{
        g.newPolygon(polygonVertices, 0);
        poligono = g.getPolygon(0);
        punto = g.getPoint(poligono.Vertices[2]);
        segmento = g.getSegment(poligono.Sides[2]);
        EXPECT_EQ(punto.Id_point, 2);
        EXPECT_EQ(punto.X, 1.5);
        EXPECT_EQ(punto.Y, 1);
        EXPECT_EQ(g.getPoint(segmento.End).Id_point, 0);
        EXPECT_EQ(g.getPoint(g.getSegment(poligono.Sides[1]).Start).X, 3);
    }
    catch(const exception exception){
        FAIL();
    }

    try {
        g.newPolygon(polygonVertices, 0);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Id is already used"));
    }

    try {
        g.newPolygon(polygonVertices, -1);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("A non negative id is required"));
    }

    polygonVertices[2] = 1;

    try {
        g.newPolygon(polygonVertices, 1);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Invalid vertices. Polygon was not craeted"));
    }

    polygonVertices.pop_back();

    try {
        g.newPolygon(polygonVertices, 1);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Three points are required to define a polygon"));
    }

    try {
        g.getPolygon(1);
        FAIL();
    }
    catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Polygon 1 does not exist"));
    }
}

TEST(TestGeometryLib, TestLength){
    GeometryLibrary g;
    double l;
    g.reset();

    g.newPoint(0, 0, 0);
    g.newPoint(3, 4, 1);

    g.newSegment(0, 1, 0);

    try {
        l = g.length(g.getSegment(0));
        EXPECT_EQ(l, 5);
    }  catch (const exception exception) {
        FAIL();
    }
}

TEST(TestGeometryLib, TestDot){
    GeometryLibrary g;
    g.reset();
    double a;

    g.newPoint(0, 0, 0);
    g.newPoint(3, 4, 1);
    g.newPoint(5, 6, 2);
    g.newPoint(4, -3, 3);
    g.newPoint(-4, -3, 4);

    try {
        a = g.dotProduct({0, 0, 1}, {1, 0, 2});
        EXPECT_EQ(a, 39);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.dotProduct({0, 0, 1}, {0, 0, 1});
        EXPECT_EQ(a, 25);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.dotProduct({0, 0, 1}, {2, 0, 3});
        EXPECT_EQ(a, 0);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.dotProduct({0, 0, 1}, {3, 0, 4});
        EXPECT_EQ(a, -24);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.dotProduct({4, 1, 4}, {5, 2, 3});
        EXPECT_EQ(a, 70);
    }  catch (const exception exception) {
        FAIL();
    }
}

TEST(TestGeometryLib, TestCross){
    GeometryLibrary g;
    g.reset();
    double a;

    g.newPoint(0, 0, 0);
    g.newPoint(3, 4, 1);
    g.newPoint(5, 6, 2);
    g.newPoint(4, -3, 3);
    g.newPoint(-4, -3, 4);

    try {
        a = g.crossProduct({0, 0, 1}, {1, 0, 2});
        EXPECT_EQ(a, -2);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.crossProduct({0, 0, 1}, {0, 0, 1});
        EXPECT_EQ(a, 0);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.crossProduct({0, 0, 1}, {2, 0, 3});
        EXPECT_EQ(a, -25);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.crossProduct({4, 1, 4}, {5, 2, 3});
        EXPECT_EQ(a, 56);
    }  catch (const exception exception) {
        FAIL();
    }
}

TEST(TestGeometryLib, TestParllelCheck){
    GeometryLibrary g;
    bool result;
    g.reset();

    g.newPoint(1.5, 0, 0);
    g.newPoint(3, 1.5, 1);

    g.newPoint(-16, 0.9, 2);
    g.newPoint(-11.9, 5, 3);

    g.newSegment(0, 1, 0);
    g.newSegment(2, 3, 1);

    g.newSegment(0, 3, 2);
    g.newSegment(2, 1, 3);

    try {
        result = g.parallelismCheck(g.getSegment(0), g.getSegment(1));
        EXPECT_EQ(result, true);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        result = g.parallelismCheck(g.getSegment(2), g.getSegment(3));
        EXPECT_EQ(result, false);
    }  catch (const exception exception) {
        FAIL();
    }
}

TEST(TestGeometryLib, TestIsOnSegment){
    GeometryLibrary g;
    bool result;
    g.reset();

    g.newPoint(-3, -2, 0);
    g.newPoint(3, 2, 1);

    g.newSegment(0, 1, 0);

    g.newPoint(0, 1, 2);
    g.newPoint(1.5, 1, 3);
    g.newPoint(4.5, 3, 4);

    try {
        result = g.isOnSegment(g.getPoint(2), g.getSegment(0));
        EXPECT_EQ(result, false);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        result = g.isOnSegment(g.getPoint(3), g.getSegment(0));
        EXPECT_EQ(result, true);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        result = g.isOnSegment(g.getPoint(4), g.getSegment(0));
        EXPECT_EQ(result, false);
    }  catch (const exception exception) {
        FAIL();
    }
}

TEST(TestGeometryLib, TestIntersection){
    GeometryLibrary g;
    g.reset();
    pair<int, int> a;

    g.newPoint(1, 1, 0);
    g.newPoint(5, 3, 1);
    g.newSegment(0, 1, 0);

    g.newPoint(1, 7, 2);
    g.newPoint(2, 4.5, 3);
    g.newSegment(2, 3, 1);

    g.newPoint(1, 3, 4);
    g.newSegment(2, 4, 2);

    g.newPoint(3, 5, 5);
    g.newSegment(2, 5, 3);

    g.newPoint(5, 7, 6);
    g.newSegment(2, 6, 4);

    try {
        a = g.intersection(g.getSegment(0), g.getSegment(1)); //intersezione propria + retta obliqua
        EXPECT_EQ(a.first, 0);
        EXPECT_EQ(g.getPoint(a.second).X, 3);
        EXPECT_EQ(g.getPoint(a.second).Y, 2);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.intersection(g.getSegment(0), g.getSegment(2)); //intersezione nel primo estremo + retta verticale
        EXPECT_EQ(a.first, 1);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.intersection(g.getSegment(0), g.getSegment(3)); //intersezione nel secondo estremo + retta obliqua
        EXPECT_EQ(a.first, 2);
    }  catch (const exception exception) {
        FAIL();
    }

    try {
        a = g.intersection(g.getSegment(0), g.getSegment(4)); //no intersezione + retta orizzontale
        EXPECT_EQ(a.first, -1);
    }  catch (const exception exception) {
        FAIL();
    }
}
}

#endif // __TEST_GEOMLIB_H
