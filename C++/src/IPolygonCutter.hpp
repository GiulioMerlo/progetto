#ifndef IPOLYCUT_H
#define IPOLYCUT_H
#include <vector>
#include <tuple>

using namespace std;

namespace PolyCut {

class IPolygonCutter {
public:
    virtual vector<vector<int>> CutPolygon(const vector<pair<double, double>>& points,
                                           const vector<int>& polygonVertices,
                                           const tuple<double, double, double, double>& segment,
                                           vector<pair<double, double>>& newPoints) = 0;

};
}

#endif // IPOLYCUT_H
