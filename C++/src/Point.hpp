#ifndef POINT_H
#define POINT_H
#include <iostream>

using namespace std;

namespace GeometryLib {

class Point {
public:
    int Id_point;
    double X;
    double Y;
};
}

#endif // POINT_H
