#ifndef IGEOMLIB_H
#define IGEOMLIB_H
#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"

namespace GeometryLib {

class IGeometryLibrary {
public:
    virtual void reset() = 0;

    virtual double length(const Segment& segment) const = 0;
    virtual double dotProduct(const Segment& s1, const Segment& s2) const = 0;
    virtual double crossProduct(const Segment& s1, const Segment& s2) const = 0;
    virtual bool parallelismCheck(const Segment& s1, const Segment& s2) const = 0;
    virtual int isOnSegment(const Point& point, const Segment& segment) const = 0;
    virtual pair<int, int> intersection(const Segment& segment, const Segment& straight) = 0;

    virtual void newPoint(const double& x, const double& y, const int& id) = 0;
    virtual void newSegment(const int& start, const int& end, const int& id) = 0;
    virtual void newPolygon(const vector<int>& points, const int& id) = 0;

    virtual const Point& getPoint(const int& id_point) const = 0;
    virtual const Segment& getSegment(const int& id_segment) const = 0;
    virtual const Polygon& getPolygon(const int& id_polygon) const = 0;

    virtual int numberPoints() const = 0;
    virtual int numberSegments() const = 0;
    virtual int numberPolygons() const = 0;
};
}

#endif // IGEOMLIB_H
