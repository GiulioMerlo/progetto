#include "PolygonCutter.hpp"
#define DEFAULT_KEY (-50000)
#include <stack>
#include <map>
using namespace GeometryLib;
namespace PolyCut {

///Pass polygon and straight points in addition to an empty container (newPoints) to store new points
vector<vector<int> > PolygonCutter::CutPolygon(const vector<pair<double, double> > &points,
                                               const vector<int> &polygonVertices,
                                               const tuple<double, double, double, double> &segment,
                                               vector<pair<double, double> > &newPoints)
{
    //LOADING DATA: forse meglio implementare un metodo apposito (?)
    for (unsigned int i = 0; i < points.size(); i++){
        _geometryLib.newPoint(points[i].first, points[i].second, polygonVertices[i]);
    }
    int polygon_id = _geometryLib.numberPolygons();
    _geometryLib.newPolygon(polygonVertices, polygon_id);

    int n = _geometryLib.numberPoints();
    int straight_id = _geometryLib.numberSegments();
    _geometryLib.newPoint(get<0>(segment), get<1>(segment), n);
    n++;
    _geometryLib.newPoint(get<2>(segment), get<3>(segment), n);
    _geometryLib.newSegment(n - 1, n, straight_id);
    //END LOADING DATA


    Polygon polygon = _geometryLib.getPolygon(polygon_id);
    Segment straight = _geometryLib.getSegment(straight_id);
    Segment side;
    Segment tmp_segment;
    int numberSides = polygon.Sides.size();
    tmp_segment.Id_segment = -1;
    vector<double> keys;
    double key;
    map<double,pair<int, int>>::iterator iter1;
    map<double,pair<int, int>>::iterator iter2;
    vector<vector<int>> cuttedPolygons;
    pair<int, int> intersection_result;
    pair<double, double> coordinates;
    int next_poly = 1;
    int writing_on = 0;
    map<double, pair<int, int>> intersections;
    stack<tuple<int, int, int>> last_intersection;

    //Prepare newPoints and start filling it with data points
    newPoints.reserve(2 * numberSides);

    //vertices
    for(int i = 0; i < numberSides; i++){
        coordinates.first = _geometryLib.getPoint(polygon.Vertices[i]).X;
        coordinates.second = _geometryLib.getPoint(polygon.Vertices[i]).Y;
        newPoints.push_back(coordinates);
    }

    //segment points
    coordinates.first = _geometryLib.getPoint(straight.Start).X;
    coordinates.second = _geometryLib.getPoint(straight.Start).Y;
    newPoints.push_back(coordinates);
    coordinates.first = _geometryLib.getPoint(straight.End).X;
    coordinates.second = _geometryLib.getPoint(straight.End).Y;
    newPoints.push_back(coordinates);

    //prepare cuttedPolygon vector
    cuttedPolygons.push_back(vector<int>());
    cuttedPolygons[0].reserve(numberSides);

    //prepare temp segment to measure distance
    tmp_segment.Start = straight.Start;
    keys.resize(numberSides);

    //pre-load keys vector
    for(auto i = 0; i < (int)keys.size(); i++){
        keys[i] = DEFAULT_KEY;
    }

    //first polygon scan: acquire intersections and order them according to signed distance (dotProduct)
    for(int i = 0; i < numberSides; i++){
        side = _geometryLib.getSegment(polygon.Sides[i]);
        if (!_geometryLib.parallelismCheck(side, straight)){
            intersection_result = _geometryLib.intersection(side, straight);
            switch (intersection_result.first){
            case -1:
                //no intersection -> do nothing
                break;

            case 0:
                //proper intersection -> save new point, intersection type and associate key to polygon side
                tmp_segment.End = intersection_result.second;
                key = _geometryLib.dotProduct(straight, tmp_segment);
                keys[i] = key;
                intersections[key] = pair<int, int>(intersection_result.first, intersection_result.second);
                break;

            case 1:
                //intersection in start point -> save intersection type and associate key to polygon side
                if(_geometryLib.crossProduct(side, straight) * _geometryLib.crossProduct(_geometryLib.getSegment(polygon.Sides[(i - 1)% numberSides]), straight) > 0){
                    tmp_segment.End = side.Start;
                    key = _geometryLib.dotProduct(straight, tmp_segment);
                    keys[i] = key;
                    intersections[key] = pair<int, int>(intersection_result.first, side.Start);
                }
                break;

            case 2:
                if(_geometryLib.crossProduct(side, straight) * _geometryLib.crossProduct(_geometryLib.getSegment(polygon.Sides[(i + 1)% numberSides]), straight) < 0){
                    tmp_segment.End = side.End;
                    key = _geometryLib.dotProduct(straight, tmp_segment);
                    keys[i] = key;
                    intersections[key] = pair<int, int>(intersection_result.first, side.End);
                }
                break;
            }
        }
    }

    //second scan: sort points in cutted polygons
    for (int i = 0; i < numberSides; i++){
        side = _geometryLib.getSegment(polygon.Sides[i]);
        if (keys[i] == DEFAULT_KEY){
            //Default key is found -> no intersections occurring on this side
            cuttedPolygons[writing_on].push_back(side.Start);
        }
        else {
            //use the key to retrive intersection's info
            intersection_result = intersections[keys[i]];
            switch (intersection_result.first) {
            case 0:
                //it is a proper intersection
                coordinates.first = _geometryLib.getPoint(intersection_result.second).X;
                coordinates.second = _geometryLib.getPoint(intersection_result.second).Y;
                newPoints.push_back(coordinates);//add new point to the newPoints collection

                //add start of the segment and intersection point to the polygon
                cuttedPolygons[writing_on].push_back(side.Start);
                cuttedPolygons[writing_on].push_back(intersection_result.second);

                //sort new point
                if (writing_on == 0){

                    //it is an intersection first found on the starting polygon
                    last_intersection.push(tuple<int, int, int>(intersection_result.second, writing_on, side.Id_segment));
                    cuttedPolygons.push_back(vector<int>());
                    cuttedPolygons[next_poly].reserve(numberSides);
                    writing_on = next_poly;
                    next_poly ++;
                    cuttedPolygons[writing_on].push_back(intersection_result.second);
                    //a new secondary polygon has been generated, now the priority is to attempt to close it in a proper way
                }
                else {
                    //it is an intersection first found on a secondary polygon
                    tmp_segment.Start = get<0>(last_intersection.top());
                    tmp_segment.End = intersection_result.second;

                    //if the obtained polygon is valid and his vertices rotate counter-clockwise then you close the secondary polygon,
                    //otherwise you open a new secondary polygon and attempt to close it first
                    iter1 = intersections.find(keys[i]);
                    iter1 --;
                    iter2 = intersections.find(keys[i]);
                    iter2 ++;
                    //note: in case intersection is at the beginning or at the end of the map, one of the previous iter will not be well
                    //defined, however the following if is able to work around that

                    if(_geometryLib.crossProduct(_geometryLib.getSegment(get<2>(last_intersection.top())), tmp_segment) > 0
                            && (get<0>(last_intersection.top()) == iter1 -> second.second ||
                                get<0>(last_intersection.top()) == iter2 -> second.second)){
                        //closure is valid -> close secondary polygon and go up one level in the opened poly gons you have at the moment

                        //sort the straight points
                        switch (_geometryLib.isOnSegment(_geometryLib.getPoint(straight.Start), tmp_segment) +
                                2 * _geometryLib.isOnSegment(_geometryLib.getPoint(straight.End), tmp_segment)) {

                        case 0:
                            //both points are not on the new segment -> do nothing
                            break;

                        case 1:
                            //only starting point is on segment
                            cuttedPolygons[writing_on].push_back(straight.Start);
                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                            break;

                        case 2:
                            //only ending point is on segment
                            cuttedPolygons[writing_on].push_back(straight.End);
                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                            break;

                        case 3:
                            //both points are on segment -> choose the order based on distanc
                            if(_geometryLib.length({-1, intersection_result.second, straight.Start}) <
                                    _geometryLib.length({-1, intersection_result.second, straight.End}))
                            {
                                //start is nearest
                                cuttedPolygons[writing_on].push_back(straight.Start);
                                cuttedPolygons[writing_on].push_back(straight.End);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                            }
                            else{

                                //end is nearest
                                cuttedPolygons[writing_on].push_back(straight.End);
                                cuttedPolygons[writing_on].push_back(straight.Start);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                            }
                            break;

                        }

                        //finish closing the polygon and go up a level
                        writing_on = get<1>(last_intersection.top());
                        last_intersection.pop();
                        cuttedPolygons[writing_on].push_back(intersection_result.second);
                    }
                    else{
                        //previous polygon couldn't be closed -> create new secondary polygon, with maximum priority to be closed
                        last_intersection.push(tuple<int, int, int>(intersection_result.second, writing_on, side.Id_segment));
                        cuttedPolygons.push_back(vector<int>());
                        cuttedPolygons[next_poly].reserve(numberSides);
                        writing_on = next_poly;
                        next_poly ++;
                        cuttedPolygons[writing_on].push_back(intersection_result.second);
                    }
                }
                break;

            case 1:
                cuttedPolygons[writing_on].push_back(side.Start);

                if(writing_on == 0){

                    //if you were on starting polygon  -> you entered a secondary one
                    last_intersection.push(tuple<int, int, int>(side.Start, writing_on, side.Id_segment));
                    cuttedPolygons.push_back(vector<int>());
                    cuttedPolygons[next_poly].reserve(numberSides);
                    writing_on = next_poly;
                    next_poly ++;
                    cuttedPolygons[writing_on].push_back(side.Start);
                }

                else{
                    //it is an intersection first found on a secondary polygon
                    tmp_segment.Start = get<0>(last_intersection.top());
                    tmp_segment.End = side.Start;

                    //if the obtained polygon is valid and his vertices rotate counter-clockwise then you close the secondary polygon,
                    //otherwise you open a new secondary polygon and attempt to close it first
                    iter1 = intersections.find(keys[i]);
                    iter1 --;
                    iter2 = intersections.find(keys[i]);
                    iter2 ++;
                    //note: in case intersection is at the beginning or at the end of the map, one of the previous iter will not be
                    //well defined, however the following if is able to work around that

                    if(_geometryLib.crossProduct(_geometryLib.getSegment(get<2>(last_intersection.top())), tmp_segment) > 0
                            && (get<0>(last_intersection.top()) == iter1 -> second.second ||
                                get<0>(last_intersection.top()) == iter2 -> second.second)){
                        //closure is valid -> close secondary polygon and go up one level in the opened polyognos
                        //you have at the moment

                        switch (_geometryLib.isOnSegment(_geometryLib.getPoint(straight.Start), tmp_segment) +
                                2 * _geometryLib.isOnSegment(_geometryLib.getPoint(straight.End), tmp_segment)) {

                        case 0:
                            //both points are not on the new segment -> do nothing
                            break;

                        case 1:
                            //only starting point is on segment
                            cuttedPolygons[writing_on].push_back(straight.Start);
                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                            break;

                        case 2:
                            //only ending point is on segment
                            cuttedPolygons[writing_on].push_back(straight.End);
                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                            break;

                        case 3:
                            //both points are on segment -> choose the order based on distance
                            if(_geometryLib.length({-1, intersection_result.second, straight.Start}) <
                                    _geometryLib.length({-1, intersection_result.second, straight.End}))
                            {
                                //start is nearest
                                cuttedPolygons[writing_on].push_back(straight.Start);
                                cuttedPolygons[writing_on].push_back(straight.End);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                            }
                            else{

                                //end is nearest
                                cuttedPolygons[writing_on].push_back(straight.End);
                                cuttedPolygons[writing_on].push_back(straight.Start);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                            }
                            break;

                        }
                        //finish closing the polygon and go up a level
                        writing_on = get<1>(last_intersection.top());
                        last_intersection.pop();
                        cuttedPolygons[writing_on].push_back(side.Start);
                    }
                    else{
                        //previous polygon couldn't be closed -> create new secondary polygon, with maximum priority to be closed
                        last_intersection.push(tuple<int, int, int>(side.Start, writing_on, side.Id_segment));
                        cuttedPolygons.push_back(vector<int>());
                        cuttedPolygons[next_poly].reserve(numberSides);
                        writing_on = next_poly;
                        next_poly ++;
                        cuttedPolygons[writing_on].push_back(side.Start);
                    }
                }
                break;

            case 2:
                cuttedPolygons[writing_on].push_back(side.Start);
                cuttedPolygons[writing_on].push_back(side.End);
                tmp_segment.Start = side.Start;
                tmp_segment.End = _geometryLib.getSegment(polygon.Sides[(i + 1) % numberSides]).End;
                if (_geometryLib.crossProduct(side, tmp_segment) < 0){
                    if (writing_on == 0){
                        //it is a double intersection -> open two new polygons
                        //first polygon
                        last_intersection.push(tuple<int, int, int>(side.End, writing_on, side.Id_segment));
                        cuttedPolygons.push_back(vector<int>());
                        cuttedPolygons[next_poly].reserve(numberSides);
                        writing_on = next_poly;
                        next_poly ++;
                        cuttedPolygons[writing_on].push_back(side.End);

                        //second polygon
                        last_intersection.push(tuple<int, int, int>(side.End, writing_on, polygon.Sides[(i +1)% numberSides]));
                        cuttedPolygons.push_back(vector<int>());
                        cuttedPolygons[next_poly].reserve(numberSides);
                        writing_on = next_poly;
                        next_poly ++;
                        cuttedPolygons[writing_on].push_back(side.End);
                    }

                    else {
                        tmp_segment.Start = get<0>(last_intersection.top());
                        tmp_segment.End = side.End;
                        //if the obtained polygon is valid and his vertices rotate counter-clockwise then you close the secondary polygon,
                        //otherwise you open a new secondary polygon and attempt to close it first
                        iter1 = intersections.find(keys[i]);
                        iter1 --;
                        iter2 = intersections.find(keys[i]);
                        iter2 ++;
                        //note: in case intersection is at the beginning or at the end of the map, one of the previous iter will not be
                        //well defined, however the following if is able to work around that

                        if(_geometryLib.crossProduct(_geometryLib.getSegment(get<2>(last_intersection.top())), tmp_segment) > 0
                                && (get<0>(last_intersection.top()) == iter1 -> second.second ||
                                    get<0>(last_intersection.top()) == iter2 -> second.second)){
                            //closure is valid -> close secondary polygon and go up one level in the opened polyognos
                            //you have at the moment

                            switch (_geometryLib.isOnSegment(_geometryLib.getPoint(straight.Start), tmp_segment) +
                                    2 * _geometryLib.isOnSegment(_geometryLib.getPoint(straight.End), tmp_segment)) {

                            case 0:
                                //both points are not on the new segment -> do nothing
                                break;

                            case 1:
                                //only starting point is on segment
                                cuttedPolygons[writing_on].push_back(straight.Start);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                break;

                            case 2:
                                //only ending point is on segment
                                cuttedPolygons[writing_on].push_back(straight.End);
                                cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                break;

                            case 3:
                                //both points are on segment -> choose the order based on distance
                                if(_geometryLib.length({-1, intersection_result.second, straight.Start}) <
                                        _geometryLib.length({-1, intersection_result.second, straight.End}))
                                {
                                    //start is nearest
                                    cuttedPolygons[writing_on].push_back(straight.Start);
                                    cuttedPolygons[writing_on].push_back(straight.End);
                                    cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                    cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                }
                                else{

                                    //end is nearest
                                    cuttedPolygons[writing_on].push_back(straight.End);
                                    cuttedPolygons[writing_on].push_back(straight.Start);
                                    cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                    cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                }
                                break;

                            }
                            //finish closing the polygon and go up a level
                            writing_on = get<1>(last_intersection.top());
                            last_intersection.pop();
                            cuttedPolygons[writing_on].push_back(side.End);

                            if (writing_on == 0){
                                last_intersection.push(tuple<int, int, int>(side.End, writing_on, polygon.Sides[(i +1)% numberSides]));
                                cuttedPolygons.push_back(vector<int>());
                                cuttedPolygons[next_poly].reserve(numberSides);
                                writing_on = next_poly;
                                next_poly ++;
                                cuttedPolygons[writing_on].push_back(side.End);
                            }
                            else{
                                tmp_segment.Start = get<0>(last_intersection.top());
                                tmp_segment.End = side.End;
                                //if the obtained polygon is valid and his vertices rotate counter-clockwise then you close the secondary polygon,
                                //otherwise you open a new secondary polygon and attempt to close it first
                                iter1 = intersections.find(keys[i]);
                                iter1 --;
                                iter2 = intersections.find(keys[i]);
                                iter2 ++;
                                //note: in case intersection is at the beginning or at the end of the map, one of the previous iter will not be
                                //well defined, however the following if is able to work around that
                                if(_geometryLib.crossProduct(_geometryLib.getSegment(get<2>(last_intersection.top())), tmp_segment) > 0
                                        && (get<0>(last_intersection.top()) == iter1 -> second.second ||
                                            get<0>(last_intersection.top()) == iter2 -> second.second)){
                                    switch (_geometryLib.isOnSegment(_geometryLib.getPoint(straight.Start), tmp_segment) +
                                            2 * _geometryLib.isOnSegment(_geometryLib.getPoint(straight.End), tmp_segment)) {

                                    case 0:
                                        //both points are not on the new segment -> do nothing
                                        break;

                                    case 1:
                                        //only starting point is on segment
                                        cuttedPolygons[writing_on].push_back(straight.Start);
                                        cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                        break;

                                    case 2:
                                        //only ending point is on segment
                                        cuttedPolygons[writing_on].push_back(straight.End);
                                        cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                        break;

                                    case 3:
                                        //both points are on segment -> choose the order based on distance
                                        if(_geometryLib.length({-1, intersection_result.second, straight.Start}) <
                                                _geometryLib.length({-1, intersection_result.second, straight.End}))
                                        {
                                            //start is nearest
                                            cuttedPolygons[writing_on].push_back(straight.Start);
                                            cuttedPolygons[writing_on].push_back(straight.End);
                                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                        }
                                        else{

                                            //end is nearest
                                            cuttedPolygons[writing_on].push_back(straight.End);
                                            cuttedPolygons[writing_on].push_back(straight.Start);
                                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.Start);
                                            cuttedPolygons[get<1>(last_intersection.top())].push_back(straight.End);
                                        }
                                        break;

                                    }
                                    //finish closing the polygon and go up a level
                                    writing_on = get<1>(last_intersection.top());
                                    last_intersection.pop();
                                    cuttedPolygons[writing_on].push_back(side.End);
                                }
                                else {
                                    last_intersection.push(tuple<int, int, int>(side.End, writing_on, polygon.Sides[(i +1)% numberSides]));
                                    cuttedPolygons.push_back(vector<int>());
                                    cuttedPolygons[next_poly].reserve(numberSides);
                                    writing_on = next_poly;
                                    next_poly ++;
                                    cuttedPolygons[writing_on].push_back(side.End);
                                }
                            }

                        }
                        else{
                            //first polygon
                            last_intersection.push(tuple<int, int, int>(side.End, writing_on, side.Id_segment));
                            cuttedPolygons.push_back(vector<int>());
                            cuttedPolygons[next_poly].reserve(numberSides);
                            writing_on = next_poly;
                            next_poly ++;
                            cuttedPolygons[writing_on].push_back(side.End);

                            //second polygon
                            last_intersection.push(tuple<int, int, int>(side.End, writing_on, polygon.Sides[(i +1)% numberSides]));
                            cuttedPolygons.push_back(vector<int>());
                            cuttedPolygons[next_poly].reserve(numberSides);
                            writing_on = next_poly;
                            next_poly ++;
                            cuttedPolygons[writing_on].push_back(side.End);
                        }
                    }
                }
                i++;
                break;
            }
        }
    }
    return cuttedPolygons;
}

}
