﻿#ifndef GEOMLIB_H
#define GEOMLIB_H
#include "IGeometryLibrary.hpp"
#include <unordered_map>

namespace GeometryLib {

class GeometryLibrary: public IGeometryLibrary {
private:
    unordered_map<int, Point> _points;
    unordered_map<int, Segment> _segments;
    unordered_map<int, Polygon> _polygons;
public:
    void reset();

    double length(const Segment& segment) const;
    double dotProduct(const Segment& s1, const Segment& s2) const;
    double crossProduct(const Segment& s1, const Segment& s2) const;
    bool parallelismCheck(const Segment& s1, const Segment& s2) const;
    int isOnSegment(const Point& point, const Segment& segment) const;
    pair<int, int> intersection(const Segment& segment, const Segment& straight);


    void newPoint(const double& x, const double& y, const int& id);
    void newSegment(const int& start, const int& end, const int& id);
    void newPolygon(const vector<int>& points, const int&id);

    const Point& getPoint(const int &id_point) const;
    const Segment& getSegment(const int& id_segment) const;
    const Polygon& getPolygon(const int& id_polygon) const;

    int numberPoints() const {return _points.size();};
    int numberSegments() const {return _segments.size();};
    int numberPolygons() const{return _polygons.size();};
};
}

#endif // GEOMLIB_H
