#include "GeometryLibrary.hpp"
#include <cmath>
#define TOLL_A 1e-4//da usare tra ()
#define TOLL_B 1e-8//da usare tra ()

namespace GeometryLib {

void GeometryLibrary::reset()
{
    _points.clear();
    _segments.clear();
    _polygons.clear();
}

double GeometryLibrary::length(const Segment &segment) const
{
    double delta_x = 0, delta_y = 0;
    delta_x = getPoint(segment.End).X - getPoint(segment.Start).X;
    delta_y = getPoint(segment.End).Y - getPoint(segment.Start).Y;
    return sqrt((delta_x*delta_x)+(delta_y*delta_y));

}

double GeometryLibrary::dotProduct(const Segment &s1, const Segment &s2) const
{
    double tmp_x1 = 0,tmp_x2 = 0, tmp_y1 = 0, tmp_y2 = 0;
    tmp_x1 = getPoint(s1.End).X - getPoint(s1.Start).X;
    tmp_x2 = getPoint(s2.End).X - getPoint(s2.Start).X;
    tmp_y1 = getPoint(s1.End).Y - getPoint(s1.Start).Y;
    tmp_y2 = getPoint(s2.End).Y - getPoint(s2.Start).Y;
    return (tmp_x1*tmp_x2)+(tmp_y1*tmp_y2);
}

double GeometryLibrary::crossProduct(const Segment &s1, const Segment &s2) const
{
    double tmp_x1 = 0, tmp_x2 = 0, tmp_y1 = 0, tmp_y2 = 0;
    tmp_x1 = getPoint(s1.End).X - getPoint(s1.Start).X;
    tmp_x2 = getPoint(s2.End).X - getPoint(s2.Start).X;
    tmp_y1 = getPoint(s1.End).Y - getPoint(s1.Start).Y;
    tmp_y2 = getPoint(s2.End).Y - getPoint(s2.Start).Y;
    return (tmp_x1*tmp_y2)-(tmp_x2*tmp_y1);
}

bool GeometryLibrary::parallelismCheck(const Segment &s1, const Segment &s2) const
{
    double m = 0, k = 0;
    k = crossProduct(s1, s2);
    m = k * k/(dotProduct(s1, s1)*dotProduct(s2, s2)); //più efficiente, non fa uso della radice quadrata
    if (m < ((TOLL_A) * (TOLL_A)))
        return true;
    return false;
}

int GeometryLibrary::isOnSegment(const Point &point, const Segment &segment) const
{
    double tmp_x1 = 0,tmp_y1 = 0,tmp_x2 = 0,tmp_y2 = 0,tmp_xp = 0,tmp_yp = 0, e_x = 0, e_y = 0;
    tmp_x1 = getPoint(segment.Start).X;
    tmp_x2 = getPoint(segment.End).X;
    tmp_y1 = getPoint(segment.Start).Y;
    tmp_y2 = getPoint(segment.End).Y;
    tmp_xp = point.X;
    tmp_yp = point.Y;

    if (abs(tmp_x1 - tmp_x2) > (TOLL_B) && abs(tmp_y1 - tmp_y2) > (TOLL_B)){
        e_x = (tmp_xp - tmp_x1)/(tmp_x2 - tmp_x1);
        e_y = (tmp_yp - tmp_y1)/(tmp_y2 - tmp_y1);

        if ((e_x >= -(TOLL_B) && e_x <= 1 + (TOLL_B)) && (e_y >= -(TOLL_B) && e_y <= 1 + (TOLL_B)) && abs(e_x - e_y) <= (TOLL_B)){
            return 1;
        }
        return 0;
    }

    else if (abs(tmp_x1 - tmp_x2) <= (TOLL_B) && abs(tmp_x1 - tmp_xp) <= (TOLL_B)){
        e_y = (tmp_yp - tmp_y1)/(tmp_y2 - tmp_y1);
        if ((e_y >= -(TOLL_B) && e_y <= 1 + (TOLL_B))){
            return 1;
        }
        return 0;
    }

    else if (abs(tmp_y1 - tmp_y2) <= (TOLL_B) && abs(tmp_y1 - tmp_yp) <= (TOLL_B)){
        e_x = (tmp_xp - tmp_x1)/(tmp_x2 - tmp_x1);
        if ((e_x >= -(TOLL_B) && e_x <= 1 + (TOLL_B))){
            return 1;
        }
        return 0;
    }
    return 0;

}

//Questo metodo deve sempre lavorare in coppia con parallel check altrimenti si rschiano insiemi di soluzioni infiniti o vuoti
pair<int, int> GeometryLibrary::intersection(const Segment &segment, const Segment &straight)
{
    double xs1, xs2, ys1, ys2, xa, ya, xb, yb, e, xp, yp;
    pair<int, int> result;
    int id;
    xs1 = getPoint(straight.Start).X;
    ys1 = getPoint(straight.Start).Y;
    xs2 = getPoint(straight.End).X;
    ys2 = getPoint(straight.End).Y;

    xa = getPoint(segment.Start).X;
    ya = getPoint(segment.Start).Y;
    xb = getPoint(segment.End).X;
    yb = getPoint(segment.End).Y;

    if (abs(xs2 - xs1) <= (TOLL_B)) { //retta verticale
        e = (xs1 - xa)/(xb - xa);
    }
    else if (abs(ys2 - ys1) <= (TOLL_B)){ //retta orizzontale
        e = (ys1 - ya)/(yb - ya);
    }
    else{ //retta obliqua
        e = (((ya - ys1)/(ys2 - ys1)) - ((xa - xs1)/(xs2 - xs1))) / (((xb - xa)/(xs2 - xs1)) - ((yb - ya)/(ys2 - ys1)));

    }

    if ((TOLL_B) < e && e < 1 - (TOLL_B)) {
        result.first = 0;
        //codice per intersezione propria

        id = numberPoints();
        xp = xa + (xb - xa) * e;
        yp = ya + (yb - ya) * e;
        newPoint(xp, yp, id);
        result.second = id;
        return result;
    }
    else if (e < -(TOLL_B) || e > 1 + (TOLL_B)){
        result.first = -1;
        //codice per intersezione inesistente

        result.second = -1;
        return result;
    }
    else if(-(TOLL_B) <= e && e <= (TOLL_B)){
        result.first = 1;
        //codice per intersezione nel primo estremo del segmento

        result.second = -1;
        return result;
    }
    else if(1 - (TOLL_B) <= e && e <= 1 + (TOLL_B)){
        result.first = 2;
        //codice per intersezione nel secondo estremo del segmento

        result.second = -1;
        return result;
    }
    throw runtime_error("Somthing goes wrong during intersection of segments " + to_string(segment.Id_segment)
                        + ", " + to_string(straight.Id_segment));
}

void GeometryLibrary::newPoint(const double &x, const double &y, const int &id)
{
    if(id < 0){
        throw runtime_error("A non negative id is required");
    }
    if(!_points.emplace(id, Point()).second){
        throw runtime_error("Id is already used");
    }
    _points[id].Id_point = id;
    _points[id].X = x;
    _points[id].Y = y;

}

void GeometryLibrary::newSegment(const int &start, const int &end, const int &id)
{
    Point s, e;
    if(id < 0){
        throw runtime_error("A non negative id is required");
    }

    try{
        s = getPoint(start);
        e = getPoint(end);
    }
    catch(exception){
        throw runtime_error("At least a point does not exist");
    }

    if(abs(s.X - e.X) <= (TOLL_B) && abs(s.Y - e.Y) <= (TOLL_B)){
        throw runtime_error("Two distinct points are required");
    }

    if(!_segments.emplace(id, Segment()).second){
        throw runtime_error("Id is already used");
    }
    _segments[id].Id_segment = id;
    _segments[id].Start = start;
    _segments[id].End = end;

}

void GeometryLibrary::newPolygon(const vector<int> &points, const int &id)
{
    int n = points.size();
    int id_segment = numberSegments();
    if(n < 3){
        throw runtime_error("Three points are required to define a polygon");
    }
    if(id < 0){
        throw runtime_error("A non negative id is required");
    }
    if(!_polygons.emplace(id, Polygon()).second){
        throw runtime_error("Id is already used");
    }
    _polygons[id].Vertices.resize(n);
    _polygons[id].Sides.reserve(n);
    _polygons[id].Id_polyogon = id;
    try{
        for(int i = 0; i < n; i++){
            _polygons[id].Vertices[i] = points[i];
            newSegment(points[i], points[(i+1)%n], id_segment);
            _polygons[id].Sides.push_back(id_segment);
            id_segment++;
        }
    }
    catch(exception){
        for (auto i = 0; i < _polygons[id].Sides.size(); i++){
            _segments.erase(_polygons[id].Sides[i]);
        }
        _polygons.erase(id);
        throw runtime_error("Invalid vertices. Polygon was not craeted");
    }

}

const Point &GeometryLibrary::getPoint(const int &id_point) const
{
    try {
        return _points.at(id_point);
    }
    catch (exception) {
        throw runtime_error("Point " + to_string(id_point) + " does not exist");
    }
}

const Segment &GeometryLibrary::getSegment(const int &id_segment) const
{
    try {
        return _segments.at(id_segment);
    }
    catch (exception) {
        throw runtime_error("Segment " + to_string(id_segment) + " does not exist");
    }
}

const Polygon &GeometryLibrary::getPolygon(const int &id_polygon) const
{
    try {
        return _polygons.at(id_polygon);
    }
    catch (const exception exception) {
        throw runtime_error("Polygon " + to_string(id_polygon) + " does not exist");
    }
}

}
