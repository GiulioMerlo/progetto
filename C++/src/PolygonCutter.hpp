#ifndef POLYCUT_H
#define POLYCUT_H
#include "IPolygonCutter.hpp"
#include "GeometryLibrary.hpp"

using namespace GeometryLib;
namespace PolyCut {

  class PolygonCutter: public IPolygonCutter {
  private:
      IGeometryLibrary& _geometryLib;
  public:
      PolygonCutter(IGeometryLibrary& geometryLib) : _geometryLib(geometryLib) {}
      vector<vector<int>> CutPolygon(const vector<pair<double, double>>& points,
                                     const vector<int>& polygonVertices,
                                     const tuple<double, double, double, double>& segment,
                                     vector<pair<double, double>>& newPoints);;
  };
}

#endif // POLYCUT_H
