#include "test_GeometryLibrary.hpp"
//#include "test_PolygonCutter.hpp"
#include "test_Rettangolo.hpp"
#include "test_Pentagono.hpp"
#include "test_Ottagono.hpp"
#include "test_Concavo.hpp"
#include "test_Concavo_2.hpp"
#include "test_Extra__Project2.hpp"
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
