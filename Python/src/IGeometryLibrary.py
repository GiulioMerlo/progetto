import src.Point as p
import src.Segment as s
import src.Polygon as poly


class IGeometryLibrary:
    def __init__(self):
        pass

    def reset(self):
        pass

    def length(self, segment: s.Segment) -> float:
        pass

    def dotProduct(self, s1: s.Segment, s2: s.Segment) -> float:
        pass

    def crossProduct(self, s1: s.Segment, s2: s.Segment) -> float:
        pass

    def parallelismCheck(self, s1: s.Segment, s2: s.Segment) -> bool:
        pass

    def isOnSegment(self, point: p.Point, segment: s.Segment) -> bool:
        pass

    def intersection(self, segment: s.Segment, straight: s.Segment) -> []:
        pass

    def newPoint(self, x: float, y: float, id: int):
        pass

    def newSegment(self, start: int, end: int, id: int):
        pass

    def newPolygon(self, points: [], id: int):
        pass

    def getPoint(self, id_point: int) -> p.Point:
        pass

    def getSegment(self, id_segment: int) -> s.Segment:
        pass

    def getPolygon(self, id_polygon: int) -> poly.Polygon:
        pass

    def numberPoints(self) -> int:
        pass

    def numberSegments(self) -> int:
        pass

    def numberPolygons(self) -> int:
        pass
