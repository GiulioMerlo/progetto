import src.IPolygonCutter as cut
import src.GeometryLibrary as geo


class PolygonCutter(cut.IPolygonCutter):
    def __init__(self, geometryLib: geo.GeometryLibrary):
        super().__init__()
        self._geometryLib = geometryLib

    def cutPolygon(self, points: [[float, float]], polygonVertices: [int], segment: [[float, float], [float, float]]) \
            -> [[[float, float]], [[int]]]:
        # Loading data
        for i in range(0, len(points)):
            self._geometryLib.newPoint(points[i][0], points[i][1], polygonVertices[i])

        polygon_id = self._geometryLib.numberPolygons()
        self._geometryLib.newPolygon(polygonVertices, polygon_id)

        straight_id = self._geometryLib.numberSegments()
        n = self._geometryLib.numberPoints()
        for i in range(0, len(segment)):
            self._geometryLib.newPoint(segment[i][0], segment[i][1], n + i)
        self._geometryLib.newSegment(n, n + 1, straight_id)

        polygon = self._geometryLib.getPolygon(polygon_id)
        straight = self._geometryLib.getSegment(straight_id)
        numberSides = len(polygon.Sides)
        intersection_result = []
        newPoints = []
        cutPolygons = []
        intersections = []
        last_intersection = []
        tmp_segment = geo.s.Segment()
        default_key = -50000
        keys = []
        writing_on = 0
        next_poly = 1
        l1 = geo.s.Segment()
        l2 = geo.s.Segment()

        def sort_function(e):
            return e[0]

        # Prepare newPoints and start filling it with data points
        for i in range(0, numberSides):
            newPoints.append([self._geometryLib.getPoint(polygon.Vertices[i]).X,
                              self._geometryLib.getPoint(polygon.Vertices[i]).Y])

        newPoints.append([self._geometryLib.getPoint(straight.Start).X,
                          self._geometryLib.getPoint(straight.Start).Y])
        newPoints.append([self._geometryLib.getPoint(straight.End).X,
                          self._geometryLib.getPoint(straight.End).Y])

        # prepare temp segment to measure distance
        tmp_segment.Start = straight.Start

        # pre-load keys list
        for i in range(0, numberSides):
            keys.append(default_key)

        # first polygon scan: acquire intersections and order them according to signed distance (dotProduct)
        for i in range(0, numberSides):
            side = self._geometryLib.getSegment(polygon.Sides[i])
            if not self._geometryLib.parallelismCheck(side, straight):
                intersection_result = self._geometryLib.intersection(side, straight)
                if intersection_result[0] == 0:
                    tmp_segment.End = intersection_result[1]
                    key = self._geometryLib.dotProduct(straight, tmp_segment)
                    keys[i] = key
                    intersections.append([key, intersection_result])
                    # proper intersection -> save new point, intersection type and associate key to polygon side

                elif intersection_result[0] == 1:
                    if self._geometryLib.crossProduct(side, straight) * self._geometryLib.crossProduct(
                            self._geometryLib.getSegment(polygon.Sides[i - 1]), straight) > 0:
                        tmp_segment.End = side.Start
                        key = self._geometryLib.dotProduct(straight, tmp_segment)
                        keys[i] = key
                        intersections.append([key, [intersection_result[0], side.Start]])
                        # single intersection in start point -> save intersection type and associate key to polygon side

                elif intersection_result[0] == 2:
                    if self._geometryLib.crossProduct(side, straight) * self._geometryLib.crossProduct(
                            self._geometryLib.getSegment(polygon.Sides[(i + 1) % numberSides]), straight) < 0:
                        tmp_segment.End = side.End
                        key = self._geometryLib.dotProduct(straight, tmp_segment)
                        keys[i] = key
                        intersections.append([key, [intersection_result[0], side.End]])
                        # double intersection in end point -> save intersection type and associate key to polygon side

        # the intersection are sorted according to signed distance
        intersections.sort(key=sort_function)

        cutPolygons.append([])

        # second scan: sort points in cut polygons
        i = 0
        while i < numberSides:
            side = self._geometryLib.getSegment(polygon.Sides[i])
            if keys[i] == default_key:
                cutPolygons[writing_on].append(side.Start)
                # Default key is found -> no intersections occurring on this side
                i += 1
            else:
                for j in range(0, numberSides):
                    if intersections[j][0] == keys[i]:
                        intersection_result = intersections[j][1]
                        break

                if intersection_result[0] == 0:
                    # add new point to the newPoints collection
                    newPoints.append([self._geometryLib.getPoint(intersection_result[1]).X,
                                      self._geometryLib.getPoint(intersection_result[1]).Y])

                    # add start of the segment and intersection point to the polygon
                    cutPolygons[writing_on].append(side.Start)
                    cutPolygons[writing_on].append(intersection_result[1])

                    # sort new point
                    if writing_on == 0:
                        # it is an intersection first found on the starting polygon
                        last_intersection.append([intersection_result[1], writing_on, side.Id_segment])
                        cutPolygons.append([])
                        writing_on = next_poly
                        next_poly += 1
                        cutPolygons[writing_on].append(intersection_result[1])
                        # a new secondary polygon has been generated, now the priority is to attempt to close it in a
                        # proper way
                    else:
                        # it is an intersection first found on a secondary polygon
                        tmp_segment.Start = last_intersection[-1][0]
                        tmp_segment.End = intersection_result[1]

                        # if the obtained polygon is valid and his vertices rotate counter-clockwise then you close the
                        # secondary polygon, otherwise you open a new secondary polygon and attempt to close it first
                        int1 = None
                        int2 = None
                        for j in range(0, len(intersections)):
                            if intersections[j][0] == keys[i]:
                                if j != 0:
                                    int1 = intersections[j - 1][1][1]
                                if j != len(intersections) - 1:
                                    int2 = intersections[j + 1][1][1]
                                break
                        # note: in case intersection is at the beginning or at the end of the map, one of the previous
                        # intersections will not be well defined, however the following if is able to work around that
                        if (self._geometryLib.crossProduct(self._geometryLib.getSegment(last_intersection[-1][2]),
                                                           tmp_segment)) > 0 and (last_intersection[-1][0] == int1 or
                                                                                  last_intersection[-1][0] == int2):
                            # closure is valid -> close secondary polygon and go up one level in the opened polygons
                            # you have at the moment
                            s = int(self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.Start),
                                                                  tmp_segment)) \
                                + 2 * int(self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.End),
                                                                        tmp_segment))

                            if s == 1:
                                # only starting point is on segment
                                cutPolygons[writing_on].append(straight.Start)
                                cutPolygons[last_intersection[-1][1]].append(straight.Start)

                            elif s == 2:
                                # only ending point is on segment
                                cutPolygons[writing_on].append(straight.End)
                                cutPolygons[last_intersection[-1][1]].append(straight.End)

                            elif s == 3:
                                # both points are on segment -> choose the order based on distance
                                l1.Start = intersection_result[1]
                                l1.End = straight.Start
                                l2.Start = intersection_result[1]
                                l2.End = straight.End
                                if self._geometryLib.length(l1) < self._geometryLib.length(l2):
                                    # start is nearest
                                    cutPolygons[writing_on].append(straight.Start)
                                    cutPolygons[writing_on].append(straight.End)
                                    cutPolygons[last_intersection[-1][1]].append(straight.End)
                                    cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                else:
                                    cutPolygons[writing_on].append(straight.End)
                                    cutPolygons[writing_on].append(straight.Start)
                                    cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                    cutPolygons[last_intersection[-1][1]].append(straight.End)

                            # finish closing the polygon and go up a level
                            writing_on = last_intersection[-1][1]
                            last_intersection.pop()
                            cutPolygons[writing_on].append(intersection_result[1])

                        else:
                            # previous polygon couldn't be closed -> create new secondary polygon, with maximum priority
                            # to be closed
                            last_intersection.append([intersection_result[1], writing_on, side.Id_segment])
                            cutPolygons.append([])
                            writing_on = next_poly
                            next_poly += 1
                            cutPolygons[writing_on].append(intersection_result[1])

                elif intersection_result[0] == 1:
                    cutPolygons[writing_on].append(side.Start)
                    if writing_on == 0:
                        # if you were on starting polygon  -> you entered a secondary one
                        last_intersection.append([side.Start, writing_on, side.Id_segment])
                        cutPolygons.append([])
                        writing_on = next_poly
                        next_poly += 1
                        cutPolygons[writing_on].append(side.Start)

                    else:
                        # it is an intersection first found on a secondary polygon
                        tmp_segment.Start = last_intersection[-1][0]
                        tmp_segment.End = side.Start
                        # if the obtained polygon is valid and his vertices rotate counter-clockwise then you close the
                        # secondary polygon, otherwise you open a new secondary polygon and attempt to close it first
                        int1 = None
                        int2 = None
                        for j in range(0, len(intersections)):
                            if intersections[j][0] == keys[i]:
                                if j != 0:
                                    int1 = intersections[j - 1][1][1]
                                if j != len(intersections) - 1:
                                    int2 = intersections[j + 1][1][1]
                                break

                        if (self._geometryLib.crossProduct(self._geometryLib.getSegment(last_intersection[-1][2]),
                                                           tmp_segment)) > 0 and (last_intersection[-1][0] == int1 or
                                                                                  last_intersection[-1][0] == int2):
                            # closure is valid -> close secondary polygon and go up one level in the opened polygons
                            # you have at the moment
                            s = int(self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.Start),
                                                                  tmp_segment)) \
                                + 2 * int(self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.End),
                                                                        tmp_segment))

                            if s == 1:
                                # only starting point is on segment
                                cutPolygons[writing_on].append(straight.Start)
                                cutPolygons[last_intersection[-1][1]].append(straight.Start)

                            elif s == 2:
                                # only ending point is on segment
                                cutPolygons[writing_on].append(straight.End)
                                cutPolygons[last_intersection[-1][1]].append(straight.End)

                            elif s == 3:
                                # both points are on segment -> choose the order based on distance
                                l1.Start = intersection_result[1]
                                l1.End = straight.Start
                                l2.Start = intersection_result[1]
                                l2.End = straight.End
                                if self._geometryLib.length(l1) < self._geometryLib.length(l2):
                                    # start is nearest
                                    cutPolygons[writing_on].append(straight.Start)
                                    cutPolygons[writing_on].append(straight.End)
                                    cutPolygons[last_intersection[-1][1]].append(straight.End)
                                    cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                else:
                                    cutPolygons[writing_on].append(straight.End)
                                    cutPolygons[writing_on].append(straight.Start)
                                    cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                    cutPolygons[last_intersection[-1][1]].append(straight.End)

                            # finish closing the polygon and go up a level
                            writing_on = last_intersection[-1][1]
                            last_intersection.pop()
                            cutPolygons[writing_on].append(side.Start)

                        else:
                            # previous polygon couldn't be closed -> create new secondary polygon, with maximum priority
                            # to be closed
                            last_intersection.append([side.Start, writing_on, side.Id_segment])
                            cutPolygons.append([])
                            writing_on = next_poly
                            next_poly += 1
                            cutPolygons[writing_on].append(side.Start)

                elif intersection_result[0] == 2:
                    cutPolygons[writing_on].append(side.Start)
                    cutPolygons[writing_on].append(side.End)
                    tmp_segment.Start = side.Start
                    tmp_segment.End = self._geometryLib.getSegment(polygon.Sides[(i + 1) % numberSides]).End
                    if self._geometryLib.crossProduct(side, tmp_segment) < 0:
                        if writing_on == 0:
                            # it is a double intersection -> open two new polygons
                            # first polygon
                            last_intersection.append([side.End, writing_on, side.Id_segment])
                            cutPolygons.append([])
                            writing_on = next_poly
                            next_poly += 1
                            cutPolygons[writing_on].append(side.End)

                            # second polygon
                            last_intersection.append([side.End, writing_on,
                                                      self._geometryLib.getSegment(
                                                          polygon.Sides[(i + 1) % numberSides]).Id_segment])
                            cutPolygons.append([])
                            writing_on = next_poly
                            next_poly += 1
                            cutPolygons[writing_on].append(side.End)

                        else:
                            tmp_segment.Start = last_intersection[-1][0]
                            tmp_segment.End = side.End
                            # if the obtained polygon is valid and his vertices rotate counter-clockwise then you close
                            # the secondary polygon, otherwise you open a new secondary polygon and attempt to close it
                            # first
                            int1 = None
                            int2 = None
                            for j in range(0, len(intersections)):
                                if intersections[j][0] == keys[i]:
                                    if j != 0:
                                        int1 = intersections[j - 1][1][1]
                                    if j != len(intersections) - 1:
                                        int2 = intersections[j + 1][1][1]
                                    break
                            if (self._geometryLib.crossProduct(self._geometryLib.getSegment(last_intersection[-1][2]),
                                                               tmp_segment)) > 0 and (last_intersection[-1][0] == int1
                                                                                      or
                                                                                      last_intersection[-1][0] == int2):
                                # closure is valid -> close secondary polygon and go up one level in the opened polygons
                                # you have at the moment
                                s = int(self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.Start),
                                                                      tmp_segment)) \
                                    + 2 * int(self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.End),
                                                                            tmp_segment))
                                if s == 1:
                                    # only starting point is on segment
                                    cutPolygons[writing_on].append(straight.Start)
                                    cutPolygons[last_intersection[-1][1]].append(straight.Start)

                                elif s == 2:
                                    # only ending point is on segment
                                    cutPolygons[writing_on].append(straight.End)
                                    cutPolygons[last_intersection[-1][1]].append(straight.End)

                                elif s == 3:
                                    # both points are on segment -> choose the order based on distance
                                    l1.Start = intersection_result[1]
                                    l1.End = straight.Start
                                    l2.Start = intersection_result[1]
                                    l2.End = straight.End
                                    if self._geometryLib.length(l1) < self._geometryLib.length(l2):
                                        # start is nearest
                                        cutPolygons[writing_on].append(straight.Start)
                                        cutPolygons[writing_on].append(straight.End)
                                        cutPolygons[last_intersection[-1][1]].append(straight.End)
                                        cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                    else:
                                        cutPolygons[writing_on].append(straight.End)
                                        cutPolygons[writing_on].append(straight.Start)
                                        cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                        cutPolygons[last_intersection[-1][1]].append(straight.End)

                                # finish closing the polygon and go up a level
                                writing_on = last_intersection[-1][1]
                                last_intersection.pop()
                                cutPolygons[writing_on].append(side.End)

                                if writing_on == 0:
                                    last_intersection.append([side.End, writing_on,
                                                              self._geometryLib.getSegment(
                                                                  polygon.Sides[(i + 1) % numberSides]).Id_segment])
                                    cutPolygons.append([])
                                    writing_on = next_poly
                                    next_poly += 1
                                    cutPolygons[writing_on].append(side.End)
                                else:
                                    tmp_segment.Start = last_intersection[-1][0]
                                    tmp_segment.End = side.End
                                    # if the obtained polygon is valid and his vertices rotate counter-clockwise then
                                    # you close the secondary polygon, otherwise you open a new secondary polygon and
                                    # attempt to close it first
                                    int1 = None
                                    int2 = None
                                    for j in range(0, len(intersections)):
                                        if intersections[j][0] == keys[i]:
                                            if j != 0:
                                                int1 = intersections[j - 1][1][1]
                                            if j != len(intersections) - 1:
                                                int2 = intersections[j + 1][1][1]
                                            break
                                    if (self._geometryLib.crossProduct(
                                            self._geometryLib.getSegment(last_intersection[-1][2]),
                                            tmp_segment)) > 0 and (last_intersection[-1][0] == int1
                                                                   or
                                                                   last_intersection[-1][0] == int2):
                                        # closure is valid -> close secondary polygon and go up one level in the opened
                                        # polygons you have at the moment
                                        s = int(
                                            self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.Start),
                                                                          tmp_segment)) \
                                            + 2 * int(
                                            self._geometryLib.isOnSegment(self._geometryLib.getPoint(straight.End),
                                                                          tmp_segment))
                                        if s == 1:
                                            # only starting point is on segment
                                            cutPolygons[writing_on].append(straight.Start)
                                            cutPolygons[last_intersection[-1][1]].append(straight.Start)

                                        elif s == 2:
                                            # only ending point is on segment
                                            cutPolygons[writing_on].append(straight.End)
                                            cutPolygons[last_intersection[-1][1]].append(straight.End)

                                        elif s == 3:
                                            # both points are on segment -> choose the order based on distance
                                            l1.Start = intersection_result[1]
                                            l1.End = straight.Start
                                            l2.Start = intersection_result[1]
                                            l2.End = straight.End
                                            if self._geometryLib.length(l1) < self._geometryLib.length(l2):
                                                # start is nearest
                                                cutPolygons[writing_on].append(straight.Start)
                                                cutPolygons[writing_on].append(straight.End)
                                                cutPolygons[last_intersection[-1][1]].append(straight.End)
                                                cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                            else:
                                                cutPolygons[writing_on].append(straight.End)
                                                cutPolygons[writing_on].append(straight.Start)
                                                cutPolygons[last_intersection[-1][1]].append(straight.Start)
                                                cutPolygons[last_intersection[-1][1]].append(straight.End)

                                        # finish closing the polygon and go up a level
                                        writing_on = last_intersection[-1][1]
                                        last_intersection.pop()
                                        cutPolygons[writing_on].append(side.End)
                                    else:
                                        last_intersection.append([side.End, writing_on,
                                                                  self._geometryLib.getSegment(
                                                                      polygon.Sides[(i + 1) % numberSides]).Id_segment])
                                        cutPolygons.append([])
                                        writing_on = next_poly
                                        next_poly += 1
                                        cutPolygons[writing_on].append(side.End)

                            else:
                                # first polygon
                                last_intersection.append([side.End, writing_on, side.Id_segment])
                                cutPolygons.append([])
                                writing_on = next_poly
                                next_poly += 1
                                cutPolygons[writing_on].append(side.End)

                                # second polygon
                                last_intersection.append([side.End, writing_on,
                                                          self._geometryLib.getSegment(
                                                              polygon.Sides[(i + 1) % numberSides]).Id_segment])
                                cutPolygons.append([])
                                writing_on = next_poly
                                next_poly += 1
                                cutPolygons[writing_on].append(side.End)
                    i += 1
                i += 1

        return [newPoints, cutPolygons]
