import src.IGeometryLibrary as geo
import src.Point as p
import src.Segment as s
import src.Polygon as poly
import math as m


class GeometryLibrary (geo.IGeometryLibrary):
    def __init__(self):
        super().__init__()
        self._points = {}
        self._segments = {}
        self._polygons = {}

    def reset(self):
        self._points.clear()
        self._segments.clear()
        self._polygons.clear()

    def length(self, segment: s.Segment) -> float:
        tmp_x = self.getPoint(segment.End).X-self.getPoint(segment.Start).X
        tmp_y = self.getPoint(segment.End).Y-self.getPoint(segment.Start).Y
        len = m.sqrt((tmp_x*tmp_x)+(tmp_y*tmp_y))
        return len

    def dotProduct(self, s1: s.Segment, s2: s.Segment) -> float:
        tmp_x1 = self.getPoint(s1.End).X - self.getPoint(s1.Start).X
        tmp_x2 = self.getPoint(s2.End).X - self.getPoint(s2.Start).X
        tmp_y1 = self.getPoint(s1.End).Y - self.getPoint(s1.Start).Y
        tmp_y2 = self.getPoint(s2.End).Y - self.getPoint(s2.Start).Y
        dot_product = (tmp_x1*tmp_x2)+(tmp_y1*tmp_y2)
        return dot_product

    def crossProduct(self, s1: s.Segment, s2: s.Segment) -> float:
        tmp_x1 = self.getPoint(s1.End).X - self.getPoint(s1.Start).X
        tmp_x2 = self.getPoint(s2.End).X - self.getPoint(s2.Start).X
        tmp_y1 = self.getPoint(s1.End).Y - self.getPoint(s1.Start).Y
        tmp_y2 = self.getPoint(s2.End).Y - self.getPoint(s2.Start).Y
        cross_product = ((tmp_x1 * tmp_y2) - (tmp_x2 * tmp_y1))
        return cross_product

    def parallelismCheck(self, s1: s.Segment, s2: s.Segment) -> bool:
        toll = 10 ** -4
        k = self.crossProduct(s1, s2)
        m = (k ** 2)/(self.dotProduct(s1, s1)*self.dotProduct(s2, s2))
        if m < (toll ** 2):
            parallelism_check = True
        else:
            parallelism_check = False
        return parallelism_check

    def isOnSegment(self, point: p.Point, segment: s.Segment) -> bool:
        toll = 10 ** -8
        tmp_x1 = self.getPoint(segment.Start).X
        tmp_x2 = self.getPoint(segment.End).X
        tmp_y1 = self.getPoint(segment.Start).Y
        tmp_y2 = self.getPoint(segment.End).Y
        tmp_xp = point.X
        tmp_yp = point.Y

        if abs(tmp_x1 - tmp_x2) > toll and abs(tmp_y1 - tmp_y2) > toll:
            e_x = (tmp_xp - tmp_x1)/(tmp_x2 - tmp_x1)
            e_y = (tmp_yp - tmp_y1)/(tmp_y2 - tmp_y1)

            if (-toll <= e_x <= 1 + toll) and (-toll <= e_y <= 1 + toll) and abs(e_x - e_y) <= toll:
                return True
            return False
        elif abs(tmp_x1 - tmp_x2) <= toll and abs(tmp_x1 - tmp_xp) <= toll:
            e_y = (tmp_yp - tmp_y1) / (tmp_y2 - tmp_y1)
            if -toll <= e_y <= 1 + toll:
                return True
            return False
        elif abs(tmp_y1 - tmp_y2) <= toll and abs(tmp_y1 - tmp_yp) <= toll:
            e_x = (tmp_xp - tmp_x1) / (tmp_x2 - tmp_x1)
            if -toll <= e_x <= 1 + toll:
                return True
            return False
        return False



    def intersection(self, segment: s.Segment, straight: s.Segment) -> []:
        toll = 10 ** -8
        result = []

        xs1 = self.getPoint(straight.Start).X
        ys1 = self.getPoint(straight.Start).Y
        xs2 = self.getPoint(straight.End).X
        ys2 = self.getPoint(straight.End).Y

        xa = self.getPoint(segment.Start).X
        ya = self.getPoint(segment.Start).Y
        xb = self.getPoint(segment.End).X
        yb = self.getPoint(segment.End).Y

        if abs(xs2 - xs1) <= toll: #retta verticale
            e = (xs1 - xa) / (xb - xa)
        elif abs(ys2 - ys1) <= toll: #retta orizzontale
            e = (ys1 - ya) / (yb - ya)
        else: #retta obliqua
            e = (((ya - ys1) / (ys2 - ys1)) - ((xa - xs1) / (xs2 - xs1))) / \
                (((xb - xa) / (xs2 - xs1)) - ((yb - ya) / (ys2 - ys1)))

        if toll < e < 1 - toll:
            result.append(0)
            #codice per intersezione propria

            id = self.numberPoints()
            xp = xa + (xb - xa) * e
            yp = ya + (yb - ya) * e
            self.newPoint(xp, yp, id)
            result.append(id)
            return result

        elif e < -toll or e > 1 + toll:
            result.append(-1)
            #codice per intersezione inesistente

            result.append(-1)
            return result

        elif -toll <= e <= toll:
            result.append(1)
            # codice per intersezione nel primo estremo del segmento

            result.append(-1)
            return result

        elif 1 - toll <= e <= 1 + toll:
            result.append(2)
            # codice per intersezione nel primo estremo del segmento

            result.append(-1)
            return result

        raise RuntimeError("Somthing goes wrong during intersection of segments " + str(segment.Id_segment) + ", "
                           + str(straight.Id_segment))

    def newPoint(self, x: float, y: float, id: int):
        if id < 0:
            raise RuntimeError("A non negative id is required")
        if id in self._points.keys():
            raise RuntimeError("Id is already used")
        self._points[id] = p.Point()
        self._points[id].X = x
        self._points[id].Y = y
        self._points[id].Id_point = id

    def newSegment(self, start: int, end: int, id: int):
        if id < 0:
            raise RuntimeError("A non negative id is required")

        try:
            st = self.getPoint(start)
            e = self.getPoint(end)
        except Exception as ex:
            raise RuntimeError("At least a point does not exist")

        if st.X == e.X and st.Y == e.Y:
            raise RuntimeError("Two distinct points are required")

        if id in self._segments.keys():
            raise RuntimeError("Id is already used")

        self._segments[id] = s.Segment()
        self._segments[id].Id_segment = id
        self._segments[id].Start = start
        self._segments[id].End = end

    def newPolygon(self, points: [], id: int):
        n = len(points)
        id_segment = self.numberSegments()

        if n < 3:
            raise RuntimeError("Three points are required to define a polygon")

        if id < 0:
            raise RuntimeError("A non negative id is required")

        if id in self._polygons.keys():
            raise RuntimeError("Id is already used")

        self._polygons[id] = poly.Polygon()
        self._polygons[id].Id_polygon = id

        try:
            for i in range (0, n):
                self._polygons[id].Vertices.append(points[i])
                self.newSegment(points[i], points[(i + 1)%n], id_segment)
                self._polygons[id].Sides.append(id_segment)
                id_segment = id_segment + 1
        except Exception as ex:
            for i in range (0, len(self._polygons[id].Sides)):
                self._segments.pop(self._polygons[id].Sides[i])
            self._polygons.pop(id)
            raise  RuntimeError("Invalid vertices. Polygon was not created")

    def getPoint(self, id_point: int) -> p.Point:
        if id_point in self._points.keys():
            return self._points[id_point]
        raise RuntimeError("Point " + str(id_point) + " does not exist")

    def getSegment(self, id_segment: int) -> s.Segment:
        if id_segment in self._segments.keys():
            return self._segments[id_segment]
        raise RuntimeError("Segment " + str(id_segment) + " does not exist")

    def getPolygon(self, id_polygon: int) -> poly.Polygon:
        if id_polygon in self._polygons.keys():
            return self._polygons[id_polygon]
        raise RuntimeError("Polygon " + str(id_polygon) + " does not exist")

    def numberPoints(self) -> int:
        return len(self._points)

    def numberSegments(self) -> int:
        return len(self._segments)

    def numberPolygons(self) -> int:
        return len(self._polygons)
