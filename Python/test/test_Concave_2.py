from unittest import TestCase
import src.PolygonCutter as cut


class TestConcave2(TestCase):
    def test(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[4, 5], [1, 4], [3, 2], [2, 0], [4,2], [2,4]]
        polygonVertices = [0, 1, 2, 3, 4, 5]
        segment = [[2.5, 3], [2.5, -1]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("New Points = ")
        print(newPoints)
        print("Cut Polygons = ")
        print(cutPolygons)
