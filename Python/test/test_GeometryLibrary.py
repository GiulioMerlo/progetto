from unittest import TestCase
import src.GeometryLibrary as geo


class TestGeometryLibrary(TestCase):
    def test_Point(self):
        g = geo.GeometryLibrary()
        g.reset()
        try:
            g.newPoint(2.0, -3.4, 0)
            g.newPoint(-4.2, 1.5, 1)
            punto0 = g.getPoint(0)
            punto1 = g.getPoint(1)
            self.assertEqual(punto0.X, 2.0)
            self.assertEqual(punto0.Y, -3.4)
            self.assertEqual(punto0.Id_point, 0)
            self.assertEqual(punto1.X, -4.2)
            self.assertEqual(punto1.Y, 1.5)
            self.assertEqual(punto1.Id_point, 1)
        except Exception as ex:
            self.fail()

        try:
            g.newPoint(3.0, 5.6, -1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "A non negative id is required")

        try:
            g.newPoint(3.0, 5.6, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Id is already used")

        try:
            g.getPoint(3)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Point 3 does not exist")

    def test_Segment(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(-6.4, 1.2, 0)
        g.newPoint(9.3, 4.5, 1)
        g.newPoint(-1.3, -2.3, 2)
        g.newPoint(7.1, 1.2, 3)

        try:
            g.newSegment(0, 1, 0)
            g.newSegment(2, 3, 1)
            segmento0 = g.getSegment(0)
            segmento1 = g.getSegment(1)
            self.assertEqual(segmento0.Id_segment, 0)
            self.assertEqual(segmento0.Start, 0)
            self.assertEqual(segmento0.End, 1)
            self.assertEqual(segmento1.Id_segment, 1)
            self.assertEqual(segmento1.Start, 2)
            self.assertEqual(segmento1.End, 3)
            self.assertEqual(g.getPoint(segmento1.Start).Y, -2.3)
        except Exception as ex:
            self.fail()

        try:
            g.newSegment(0, 2, -1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "A non negative id is required")

        try:
            g.newSegment(0, 4, 2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "At least a point does not exist")

        try:
            g.newSegment(1, 1, 2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Two distinct points are required")

        try:
            g.newSegment(1, 3, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Id is already used")

        try:
            g.getSegment(2)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Segment 2 does not exist")

    def test_Polygon(self):
        g = geo.GeometryLibrary()
        g.reset()
        points = [[0.0, 0.0], [3.0, 0.0], [1.5, 1.0]]
        polygonVertices = [0, 1, 2]

        for i in range (0, len(points)):
            g.newPoint(points[i][0], points[i][1], polygonVertices[i])

        try:
            g.newPolygon(polygonVertices, 0)
            poligono = g.getPolygon(0)
            punto = g.getPoint(poligono.Vertices[2])
            segmento = g.getSegment(poligono.Sides[2])
            self.assertEqual(poligono.Id_polygon, 0)
            self.assertEqual(punto.X, g.getPoint(2).X)
            self.assertEqual(g.getPoint(segmento.Start).Y, g.getPoint(2).Y)
            self.assertEqual(g.getPoint(segmento.End).X, g.getPoint(0).X)
        except Exception as ex:
            self.fail()

        try:
            g.newPolygon(polygonVertices, 0)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Id is already used")

        try:
            g.newPolygon(polygonVertices, -1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "A non negative id is required")

        polygonVertices[2] = 1
        try:
            g.newPolygon(polygonVertices, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Invalid vertices. Polygon was not created")

        polygonVertices.pop(2)
        try:
            g.newPolygon(polygonVertices, 1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Three points are required to define a polygon")

        try:
            g.getPolygon(1)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Polygon 1 does not exist")

    def test_Length(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(0,0,0)
        g.newPoint(3,4,1)
        g.newSegment(0, 1, 0)

        try:
            l = g.length(g.getSegment(0))
            self.assertEqual(l, 5)
        except Exception as ex:
            self.fail()

    def test_Dot(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(0, 0, 0)
        g.newPoint(3, 4, 1)
        g.newPoint(5, 6, 2)
        g.newPoint(4, -3, 3)
        g.newPoint(-4, -3, 4)

        segment_1 = geo.s.Segment()
        segment_2 = geo.s.Segment()

        try:
            segment_1.Start = 0
            segment_1.End = 1
            segment_2.Start = 0
            segment_2.End = 2
            a = g.dotProduct(segment_1, segment_2)
            self.assertEqual(a, 39)
        except Exception as ex:
            self.fail()

        try:
            segment_2.End = 1
            a = g.dotProduct(segment_1, segment_2)
            self.assertEqual(a, 25)
        except Exception as ex:
            self.fail()

        try:
            segment_2.End = 3
            a = g.dotProduct(segment_1, segment_2)
            self.assertEqual(a, 0)
        except Exception as ex:
            self.fail()

        try:
            segment_2.End = 4
            a = g.dotProduct(segment_1, segment_2)
            self.assertEqual(a, -24)
        except Exception as ex:
            self.fail()

        try:
            segment_1.Start = 1
            segment_1.End = 4
            segment_2.Start = 2
            segment_2.End = 3

            a = g.dotProduct(segment_1, segment_2)
            self.assertEqual(a, 70)
        except Exception as ex:
            self.fail()

    def test_Cross(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(0, 0, 0)
        g.newPoint(3, 4, 1)
        g.newPoint(5, 6, 2)
        g.newPoint(4, -3, 3)
        g.newPoint(-4, -3, 4)

        segment_1 = geo.s.Segment()
        segment_2 = geo.s.Segment()

        try:
            segment_1.Start = 0
            segment_1.End = 1
            segment_2.Start = 0
            segment_2.End = 2
            a = g.crossProduct(segment_1, segment_2)
            self.assertEqual(a, -2)
        except Exception as ex:
            self.fail()

        try:
            segment_2.End = 1
            a = g.crossProduct(segment_1, segment_2)
            self.assertEqual(a, 0)
        except Exception as ex:
            self.fail()

        try:
            segment_2.End = 3
            a = g.crossProduct(segment_1, segment_2)
            self.assertEqual(a, -25)
        except Exception as ex:
            self.fail()

        try:
            segment_1.Start = 1
            segment_1.End = 4
            segment_2.Start = 2
            segment_2.End = 3

            a = g.crossProduct(segment_1, segment_2)
            self.assertEqual(a, 56)
        except Exception as ex:
            self.fail()

    def test_ParallelCheck(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(1.5, 0, 0)
        g.newPoint(3, 1.5, 1)

        g.newPoint(-16, 0.9, 2)
        g.newPoint(-11.9, 5, 3)

        g.newSegment(0, 1, 0)
        g.newSegment(2, 3, 1)

        g.newSegment(0, 3, 2)
        g.newSegment(2, 1, 3)

        try:
            result = g.parallelismCheck(g.getSegment(0),g.getSegment(1))
            self.assertEqual(result, True)
        except Exception as ex:
            self.fail()

        try:
            result = g.parallelismCheck(g.getSegment(2),g.getSegment(3))
            self.assertEqual(result, False)
        except Exception as ex:
            self.fail()

    def test_IsOnSegment(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(-3, -2, 0)
        g.newPoint(3, 2, 1)

        g.newSegment(0, 1, 0)

        g.newPoint(0, 1, 2)
        g.newPoint(1.5, 1, 3)
        g.newPoint(4.5, 3, 4)

        try:
            result = g.isOnSegment(g.getPoint(2), g.getSegment(0))
            self.assertEqual(result, False)
        except Exception as ex:
            self.fail()

        try:
            result = g.isOnSegment(g.getPoint(3), g.getSegment(0))
            self.assertEqual(result, True)
        except Exception as ex:
            self.fail()

        try:
            result = g.isOnSegment(g.getPoint(4), g.getSegment(0))
            self.assertEqual(result, False)
        except Exception as ex:
            self.fail()

    def test_Intersection(self):
        g = geo.GeometryLibrary()
        g.reset()

        g.newPoint(1, 1, 0)
        g.newPoint(5, 3, 1)
        g.newSegment(0, 1, 0)

        g.newPoint(1, 7, 2)
        g.newPoint(2, 4.5, 3)
        g.newSegment(2, 3, 1)

        g.newPoint(1, 3, 4)
        g.newSegment(2, 4, 2)

        g.newPoint(3, 5, 5)
        g.newSegment(2, 5, 3)

        g.newPoint(5, 7, 6)
        g.newSegment(2, 6, 4)

        try:
            a = g.intersection(g.getSegment(0), g.getSegment(1)) #intersezione propria + retta obliqua
            self.assertEqual(a[0], 0)
            self.assertEqual(g.getPoint(a[1]).X, 3)
            self.assertEqual(g.getPoint(a[1]).Y, 2)
        except Exception as ex:
            self.fail()

        try:
            a = g.intersection(g.getSegment(0), g.getSegment(2)) #intersezione nelprimo estremo + retta verticale
            self.assertEqual(a[0], 1)
        except Exception as ex:
            self.fail()

        try:
            a = g.intersection(g.getSegment(0), g.getSegment(3)) #intersezione nel secondo estremo + retta obliqua
            self.assertEqual(a[0], 2)
        except Exception as ex:
            self.fail()

        try:
            a = g.intersection(g.getSegment(0), g.getSegment(4)) #no intersezione + retta orizzontale
            self.assertEqual(a[0], -1)
        except Exception as ex:
            self.fail()
