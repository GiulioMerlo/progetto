from unittest import TestCase
import src.PolygonCutter as cut


class TestOctagon(TestCase):
    def test(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[-6, -4], [-5, -5], [-2, -5], [-1, -4], [-1, -2], [-2, -1], [-5, -2], [-6, -2]]
        polygonVertices = [0, 1, 2, 3, 4, 5, 6, 7]
        segment = [[-6, -5], [-1, -1]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("New Points =")
        print(newPoints)
        print("Cut Polygons =")
        print(cutPolygons)
