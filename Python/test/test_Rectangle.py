from unittest import TestCase
import src.PolygonCutter as cut


class TestRectangle(TestCase):
    def test(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[1, 1], [5, 1], [5, 3.1], [1, 3.1]]
        polygonVertices = [0, 1, 2, 3]
        segment = [[2.0, 1.2], [4.0, 3.0]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("New Points =")
        print(newPoints)
        print("Cut Polygons =")
        print(cutPolygons)

