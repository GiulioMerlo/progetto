from unittest import TestCase
import src.PolygonCutter as cut


class TestConcave(TestCase):
    def test(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[1.5, 1], [5.6, 1.5], [5.5, 4.8], [4.0, 6.2], [3.2, 4.22], [1.0, 4.0]]
        polygonVertices = [0, 1, 2, 3, 4, 5]
        segment = [[2.0, 3.7], [4.1, 5.9]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("New Points = ")
        print(newPoints)
        print("Cut Polygons =")
        print(cutPolygons)
