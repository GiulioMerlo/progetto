from unittest import TestCase
import src.PolygonCutter as cut


class TestPentagon(TestCase):
    def test(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[2.5, 1], [4, 2.1], [3.4, 4.2], [1.6, 4.2], [1, 2.1]]
        polygonVertices = [0, 1, 2, 3, 4]
        segment = [[1.4, 2.75], [3.6, 2.2]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("New Points =")
        print(newPoints)
        print("Cut Polygons =")
        print(cutPolygons)
