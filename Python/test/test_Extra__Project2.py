from unittest import TestCase
import src.PolygonCutter as cut


class TestExtra(TestCase):
    def test1(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[2, -2], [0, -1], [3, 1], [0, 2], [3, 2], [3, 3], [-1, 3], [-3, 1], [0, 0], [-3, -2]]
        polygonVertices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        segment = [[-4, -4], [4, 4]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("TEST 1")
        print("New Points =")
        print(newPoints)
        print("Cut Polygons =")
        print(cutPolygons)
        print()

    def test2(self):
        g = cut.geo.GeometryLibrary()
        g.reset()

        cutter = cut.PolygonCutter(g)
        points = [[2, -2], [0, -1], [3, 1], [0, 2], [3, 2], [3, 3], [-1, 3], [-3, 1], [0, 0], [-3, -2]]
        polygonVertices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        segment = [[0, -3], [0, 4]]

        [newPoints, cutPolygons] = cutter.cutPolygon(points, polygonVertices, segment)

        print("TEST 2")
        print("New Points =")
        print(newPoints)
        print("Cut Polygons =")
        print(cutPolygons)
