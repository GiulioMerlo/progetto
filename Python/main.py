import src.empty_class as empty_class
import src.GeometryLibrary as geo

if __name__ == "__main__":
    g = geo.GeometryLibrary()
    g.reset()

    def my_func(e):
        return e[1]

    intersections = [
        [1, 2],
        [2,-5],
        [3,0]
    ]

    intersections.sort(key = my_func)

    a = None
    b = None
    for i in range (0, len(intersections)):
        if intersections[i][1] == -5:
            if i != 0:
                a = intersections[i - 1][0]
            if i != len(intersections) - 1:
                b = intersections[i + 1][0]

    print(intersections)
    print(a)
    print(b)